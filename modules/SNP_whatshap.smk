__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#SNP
# haplotypes from short reads using whatshap
rule whatshap:
    input:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bamidx   = "{outdir}/mapped/{{sample}}_sorted_parsed.bam.bai".format(outdir=config["outdir"]),
        ref_index = config["REFPATH"] + "/" + config["GENOME"] + ".fai",
        bam = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"]),
        var = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
    output:
        phased = "{outdir}/variant/{{sample}}_varscan_phased.vcf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    threads: 1
    message: "Phasing variant from {input.var} using WhatsHap."
    shell:
        """
        if [ ! -s {input.var} ]
        then
                echo 'WARNING: _varscan.vcf empty'
                touch {output.phased}
                exit 0
        fi
        singularity exec {params.bind} {params.allmine_bin} \
        whatshap phase \
        -H 20 \
        --reference {input.ref} \
        -o {output.phased} \
        {input.var} \
        {input.bam}
        """
