__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

#QC
# multiQC on fastqc outputs
rule multiqc_fastqc:
    input:
        expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        #report("{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]), caption="report/multiqc.rst", category="Quality control")
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"],
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {params.outdir}/fastqc
        """
