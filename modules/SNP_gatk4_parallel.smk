################################################################################
###########################  SNP calling using  GATK ###########################
################################################################################

# 3) SNP caller using GATK4
#gatk:4 rules
# 3-1) create a reference dictionnary the ref must not be a sl and a ref.fa => ref.dict (not ref.fa.dict)
rule gatk4_ref_dict:
    input:
        ref  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME"]),
        fai  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME_FAI"]),
    output:
        dic  = "{refp}/{ref}.dict".format(refp=config["REFPATH"],ref=config["GENOME"]) #config["REFPATH"] + "/" + config["GENOME"] +".dict"
    params:
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} gatk CreateSequenceDictionary -R {input.ref}
        #singularity exec {params.bind} {params.samtools_bin} samtools faidx {input.ref}
        singularity exec {params.bind} {params.samtools_bin} samtools dict -o {output.dic} {input.ref}
        """

# ------------------  parrallel by chr -----------------
# 3-2) HaplotypeCaller for each sample and each chr
rule gatk4_hc:
    input:
        #"{outdir}/variant/chrslist.OK".format(outdir=config["outdir"]),
        bam     = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"]),
        refdict = "{refp}/{ref}.dict".format(refp=config["REFPATH"],ref=config["GENOME"]),
    output:
        gvcf="{outdir}/variant/gatk_gvcf/{{sample}}-{{mychr}}.g.vcf.gz".format(outdir=config["outdir"])
    params:
        ch="{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx24G -XX:ParallelGCThreads={threads}' HaplotypeCaller \
        --reference {params.ref} \
        --input {input.bam} \
        -ERC GVCF -L {params.ch} \
        --output {output.gvcf}
        exit 0
        """

# 3-3) Generate a map (text file) of gvcf files for each chr
rule gatk4_gvcf_map_file:
    input:
        gvcfs = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz", outdir=config["outdir"], sample=samples['SampleName']),
        fai  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME_FAI"]),
    output:
        gvcfmap      = "{outdir}/variant/gvcf_{{mychr}}_list.map".format(outdir=config["outdir"]),
    params:
        ch="{mychr}",
        csv         = expand("{sample}\t{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz", outdir=config["outdir"], sample=samples['SampleName']),
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"]
    threads: 1
    run:
        x=params.csv
        f = open(output.gvcfmap, "w")
        for i in x:
            f.write("%s\n" % (i))
        f.close()


# 3-4) build a datastore of gvcf for each chr
# see: https://gatk.broadinstitute.org/hc/en-us/articles/360040096732-GenomicsDBImport
rule gatk4_gdb:
    input:
        gvcf = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz",outdir=config["outdir"],sample=samples['SampleName']),
        gvcfmap      = "{outdir}/variant/gvcf_{{mychr}}_list.map".format(outdir=config["outdir"]),
    output:
        "{outdir}/variant/gatk_genomicsdb_{{mychr}}.ok".format(outdir=config["outdir"])
    params:
        ch="{mychr}",
        tempdir=config["outdir"] + "/variant/tmpgatkdir",
        gdb  = config["outdir"] + "/variant/" + "GenomicsDB_{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    shell:
        """
        mkdir -p {params.tempdir}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8g -Xms8g' GenomicsDBImport \
        --genomicsdb-workspace-path {params.gdb} \
        --batch-size 200 \
        -L {params.ch} \
        --sample-name-map {input.gvcfmap} \
        --reader-threads {threads} \
        --tmp-dir={params.tempdir} && touch {output}
        """


# 3-5) genotype (GenotypeGVCFs) for all samples for each chr
# https://gatk.broadinstitute.org/hc/en-us/articles/360047218551-GenotypeGVCFs
rule gatk4_gc:
    input:
        gdb="{outdir}/variant/gatk_genomicsdb_{{mychr}}.ok".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_{{mychr}}_genotyped.vcf.gz".format(outdir=config["outdir"])
        # WARNING if {outdir}/variant/gatk_{{mychr}}.vcf.gz is NOT working we need {{mychr}}_something
    params:
        tempdir=config["outdir"] + "/tmpgatkdir",
        gdb  = config["outdir"] + "/variant/" + "GenomicsDB_{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    message: "GATK4 genotype_variants vcf\n"
    shell:
        """
        mkdir -p {params.tempdir}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' GenotypeGVCFs \
        --variant gendb://{params.gdb} \
        --reference {params.ref} \
        --output {output.vcf} \
        --tmp-dir={params.tempdir}
        """

# 3-6) merge all the vcf produced by chr
rule combinevcf:
    input:
        #"{outdir}/variant/chrslist.OK".format(outdir=config["outdir"]),
        vcf=expand("{outdir}/variant/gatk_{mychr}_genotyped.vcf.gz",outdir=config["outdir"],mychr=CHRS),
        refdict = config["REFPATH"] + "/" + config["GENOME"] +".dict",
    output:
        gvcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    params:
        mlist = expand(" -I {outdir}/variant/gatk_{mychr}_genotyped.vcf.gz",outdir=config["outdir"],mychr=CHRS),
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx16G' GatherVcfs {params.mlist} -O {output.gvcf}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx16G' IndexFeatureFile -I {output.gvcf}
        #-I vcf_list_to_merge.txt
        """


# -------------------- SNPs selection and filtering --------------------------------
# 3-7) select SNPs only
rule gatk4_select_snps_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type SNP \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """

# 3-8) hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants
# QualByDepth (QD)
# FisherStrand (FS)
# StrandOddsRatio (SOR)
# RMSMappingQuality (MQ)
# MappingQualityRankSumTest (MQRankSum)
# ReadPosRankSumTest (ReadPosRankSum)
#
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531112?id=2806
# For reference, here are some basic filtering thresholds to improve upon.
# QD < 2.0
# QUAL < 30.0
# SOR > 3.0
# FS > 60.0
# MQ < 40.0
# MQRankSum < -12.5
# ReadPosRankSum < -8.0
rule gatk4_filterSnps:
    input:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.filtered_snps.vcf.gz".format(outdir=config["outdir"])
    params:
        vcftmp ="{outdir}/variant/vcf.snp.tmp.vcf.gz".format(outdir=config["outdir"]),
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"],
        QUAL_filter = config["snp_QUAL_filter"],
        QD_filter = config["snp_QD_filter"],
        FS_filter = config["snp_FS_filter"],
        MQ_filter = config["snp_MQ_filter"],
        SOR_filter = config["snp_SOR_filter"],
        MQRankSum_filter = config["snp_MQRankSum_filter"],
        ReadPosRankSum_filter = config["snp_ReadPosRankSum_filter"]
    threads: 10
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {params.vcftmp} \
        -filter-name "QUAL_filter" -filter "QUAL {params.QUAL_filter}" \
        -filter-name "QD_filter" -filter "QD {params.QD_filter}" \
        -filter-name "FS_filter" -filter "FS {params.FS_filter}" \
        -filter-name "MQ_filter" -filter "MQ {params.MQ_filter}" \
        -filter-name "SOR_filter" -filter "SOR {params.SOR_filter}" \
        -filter-name "MQRankSum_filter" -filter "MQRankSum {params.MQRankSum_filter}" \
        --genotype-filter-expression "DP < 3" \
        --genotype-filter-name "LowDP3" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum {params.ReadPosRankSum_filter}"

        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' SelectVariants \
        --variant {params.vcftmp} \
        --set-filtered-gt-to-nocall \
        --exclude-filtered \
        --reference {params.ref} \
        --output {output.vcf}
        rm -f {params.vcftmp} {params.vcftmp}.tbi
        """

# 3-9) rules for creating quality score graphs for raw SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_snps_raw_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_raw_snps.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

# 3-10) rules for creating quality score graphs for filtered SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_snps_filtered_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.filtered_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_filtered_snps.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

## 3-11) plots run Rscript to create quality score graphs to determine filters for indels vcf and snps vcf
## Rscript "graph_score_qual.R" must be download
rule gatk4_snps_quality_graphe_pdf:
    input:
        rawsnps="{outdir}/variant/gatk_all.score_raw_snps.csv".format(outdir=config["outdir"]),
        filteredsnps = "{outdir}/variant/gatk_all.score_filtered_snps.csv".format(outdir=config["outdir"])
    output:
        pdf="{outdir}/variant/gatk_all.score_snps.pdf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        R_bin = config["R_bin"]
    shell:
        """
        singularity exec {params.bind} {params.R_bin} \
        Rscript --vanilla gatk_scores_qual_raw_vs_filtered.R {input.rawsnps} {input.filteredsnps} "SNPs" {output.pdf}
        """


# ----------------------------- INDELs selection and filtering ------------------------------------
# 3-12) select INDELs only
rule gatk4_select_indels_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 10
    message: "GATK4 select INDELs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type INDEL \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """
# 3-13) hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# (https://gatk.broadinstitute.org/hc/en-us/articles/360035890471)
# "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"
rule gatk4_filterIndels:
    input:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.filtered_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"],
        QUAL_filter = config["indel_QUAL_filter"],
        QD_filter = config["indel_QD_filter"],
        FS_filter = config["indel_FS_filter"],
        ReadPosRankSum_filter = config["indel_ReadPosRankSum_filter"]
    threads: 10
    message: "GATK4 select INDELs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf} \
        -filter-name "QUAL_filter" -filter "QUAL {params.QUAL_filter}" \
        -filter-name "QD_filter" -filter "QD {params.QD_filter}" \
        -filter-name "FS_filter" -filter "FS {params.FS_filter}" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum {params.ReadPosRankSum_filter}"
        """

# 3-14) rules for creating quality score graphs for raw SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_indels_raw_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_raw_indels.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

# 3-15) rules for creating quality score graphs for filtered SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_indels_filtered_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.filtered_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_filtered_indels.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

## 3-16) plots run Rscript to create quality score graphs to determine filters for indels vcf and snps vcf
## Rscript "graph_score_qual.R" must be download
rule gatk4_indels_quality_graphe_pdf:
    input:
        rawindels="{outdir}/variant/gatk_all.score_raw_indels.csv".format(outdir=config["outdir"]),
        filteredindels = "{outdir}/variant/gatk_all.score_filtered_indels.csv".format(outdir=config["outdir"])
    output:
        pdf="{outdir}/variant/gatk_all.score_indels.pdf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        R_bin = config["R_bin"]
    shell:
        """
        singularity exec {params.bind} {params.R_bin} \
        Rscript --vanilla gatk_scores_qual_raw_vs_filtered.R {input.rawindels} {input.filteredindels} "INDELs" {output.pdf}
        """


# recalibration need vcf reference
#rule gatk4_recalibrate_calls:

#to compress & index a vcf:
#bgzip file.vcf       # or:   bcftools view file.vcf -Oz -o file.vcf.gz
#tabix file.vcf.gz    # or:   bcftools index file.vcf.gz

######################################## END GATK #################################################

