__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

#PREPROC
# adaptor removal, trim and dmerge PE read with cutadapt
rule trim_and_merge_pe:
    input:
        R1  = get_fastq1,
        R2  = get_fastq2
        #R1 = lambda wildcards: "../data/"+samples.fq1[wildcards.sample],
        #R2 = lambda wildcards: "../data/"+samples.fq2[wildcards.sample]
        #R1  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna(),
        #R2  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        #R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    message: "Running fastp on files {input.R1} and {input.R2} \n"
    params:
        fqdir         = config["fq_dir"],
        outdir        = config["outdir"],
        modules       = config["MODULES"],
        fastp_bin     = config["fastp_bin"],
        cutadapt_bin  = config["cutadapt_bin"],
        bind          = config["BIND"],
        json          = config["outdir"]+"/fastp/{sample}_trim.json",
        html          = config["outdir"]+"/fastp/{sample}_trim.html",
        tmpout        = config["outdir"]+"/fastp/{sample}",
        primers_for   = config["primers_for"],
        primers_rev   = config["primers_rev"]
    threads: 4
    shell:
        """
        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -I {input.R2} \
        -o {params.tmpout}.f1.1.fq.gz \
        -O {params.tmpout}.f1.2.fq.gz \
        --json={params.json} \
        --html={params.html} \
        --length_required 50 \
        --average_qual 20 \
        --low_complexity_filter \
        --complexity_threshold 20 \
        --thread {threads}
        rm -f {params.html} {params.json}

        singularity exec {params.bind} {params.cutadapt_bin} cutadapt \
        --cores {threads} \
        -e 0.10 \
        --action=trim \
        -g file:{params.primers_for} \
        -G file:{params.primers_rev} \
        -o {params.tmpout}.c1.1.fq.gz \
        -p {params.tmpout}.c2.1.fq.gz \
        {params.tmpout}.f1.1.fq.gz \
        {params.tmpout}.f1.2.fq.gz
        rm -f {params.tmpout}.f1.1.fq.gz {params.tmpout}.f1.2.fq.gz

        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {params.tmpout}.c1.1.fq.gz \
        -I {params.tmpout}.c2.1.fq.gz \
        --merge \
        --correction \
        --merged_out {output.R1} \
        --json={params.json} \
        --html={params.html} \
        --length_required 50 \
        --thread {threads}
        rm -f {params.html} {params.json}
        rm -f {params.tmpout}.c1.1.fq.gz {params.tmpout}.c2.1.fq.gz
        """
