__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

# Snakemake workflow for RNA-seq using star mapper 2 pass with reference reindexing.
# feature: fastq from csv file, SLURM, report
# Star 2 pass with genome reindexing
# gff3 must be converted to gtf:
#singularity exec /nosave/project/gafl/tools/containers/cufflinks_v2.2.1.sif gffread Annuum.v.2.0.gff3 -T -o Annuum.CM334.v1.6.Total.gtf
rule star_index:
    input:
        genome = config["REFPATH"] + "/" + config["GENOME"], # provide your reference FASTA file
        gtf    = config["REFPATH"] + "/" + config["GTF"] # provide your GTF file
    output:
        "{outdir}/mapping/saindex1/indexing1.OK".format(outdir=config["outdir"]) # flag if ended correctly
    threads:
        20 # set the maximum number of available cores
    params:
        bind       = config["BIND"],
        star_bin   = config["star_bin"],
        outdir     = "{outdir}/mapping/saindex1".format(outdir=config["outdir"]), # you can also rename the index folder
        maxreadlen = config["MAXREADLEN"]
    message:
        "Building star index 1 for reference genome {input.genome}\n"
    shell:
        """
        singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
        --runMode genomeGenerate \
        --genomeDir {params.outdir} \
        --genomeFastaFiles {input.genome} \
        --sjdbGTFfile {input.gtf} \
        --sjdbOverhang {params.maxreadlen} && \
        touch {output}
        """

rule star_pass1:
     input:
        R1  = get_fastq1,
        R2  = get_fastq2,
        #R1 = lambda wildcards: "../data/"+samples.fq1[wildcards.sample],
        #R2 = lambda wildcards: "../data/"+samples.fq2[wildcards.sample]
        #R1  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq1"]].dropna(),
        #R2  = lambda wildcards: config["fq_dir"]+"/"+samples.loc[(wildcards.sample), ["fq2"]].dropna()

        indexok = "{outdir}/mapping/saindex1/indexing1.OK".format(outdir=config["outdir"])
     params:
        bind       = config["BIND"],
        star_bin   = config["star_bin"],
        refdir = "{outdir}/mapping/saindex1".format(outdir=config["outdir"]),
        outdir = "{outdir}/mapping/starpass1/{{sample}}".format(outdir=config["outdir"]),
        rmbam  = "{outdir}/mapping/starpass1/{{sample}}/Aligned.out.bam".format(outdir=config["outdir"])
     output:
        "{outdir}/mapping/starpass1/{{sample}}/SJ.out.tab".format(outdir=config["outdir"])
     threads:
        10
     shell:
        """
        #--outSAMtype None
        #--outSAMtype BAM Unsorted
        rm -rf {params.outdir}
        mkdir -p {params.outdir}
        cd {params.outdir}
        singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
        --genomeDir {params.refdir} \
        --readFilesIn {input.R1} {input.R2} \
        --readFilesCommand zcat \
        --outSAMtype None && \
        touch {params.outdir}/pass1.OK
        #rm -f {params.rmbam}
        """

rule sj_filter:
    input:
       lambda wildcards: expand(expand("{outdir}/mapping/starpass1/{sample}/SJ.out.tab", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        sjfilmerged = "{outdir}/mapping/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
    params:
        sjpath  = "{outdir}/mapping/starpass1".format(outdir=config["outdir"]),
        sjlist = "{outdir}/mapping/starpass1/*/SJ.out.tab".format(outdir=config["outdir"])
    shell:
        """
        cat {params.sjlist} | awk '($5 > 0 && $7 > 2 && $6==0)' | cut -f1-6 | sort | uniq > {output}
        """

rule star_index2:
    input:
        genome   = config["REFPATH"] + "/" + config["GENOME"], # provide your reference FASTA file
        gtf      = config["REFPATH"] + "/" + config["GTF"], # provide your GTF file
        sj_pass1 = "{outdir}/mapping/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
    output:
        "{outdir}/mapping/saindex2/indexing2.OK".format(outdir=config["outdir"]) # flag if ended correctly
    threads:
        10 # set the maximum number of available cores
    params:
        bind      = config["BIND"],
        star_bin  = config["star_bin"],
        outdir    = "{outdir}/mapping/saindex2".format(outdir=config["outdir"]), # you can also rename the index folder
        maxreadlen = config["MAXREADLEN"]
    message:
        "Building star index 2 for reference genome {input.genome}\n"
    shell:
        """
        singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
        --runMode genomeGenerate \
        --genomeDir {params.outdir} \
        --genomeFastaFiles {input.genome} \
        --sjdbGTFfile {input.gtf} \
        --sjdbOverhang {params.maxreadlen} \
        --sjdbFileChrStartEnd {input.sj_pass1} && \
        touch {output}
        """

rule star_pass2:
     input:
        R1  = get_fastq1,
        R2  = get_fastq2,
        indexok = "{outdir}/mapping/saindex2/indexing2.OK".format(outdir=config["outdir"])
     params:
        bind      = config["BIND"],
        star_bin  = config["star_bin"],
        refdir = "{outdir}/mapping/saindex2".format(outdir=config["outdir"]),
        outdir = '{outdir}/mapping/starpass2/{{sample}}'.format(outdir=config["outdir"])
     output:
        "{outdir}/mapping/starpass2/{{sample}}/pass2.OK".format(outdir=config["outdir"])
     threads:
        10
     shell:
        """
        rm -rf {params.outdir}
        mkdir -p {params.outdir}
        cd {params.outdir}
        singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
        --genomeDir {params.refdir} \
        --readFilesIn {input.R1} {input.R2} \
        --readFilesCommand zcat \
        --quantMode TranscriptomeSAM GeneCounts \
        --outSAMtype BAM SortedByCoordinate && \
        touch {params.outdir}/pass2.OK
        """
