__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#SNP
#annovar SNP annotation
rule vcf4_to_avinput:
    input:
        vcf = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
    output:
        annovar ="{outdir}/variant/{{sample}}_varscan.avinput".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Converting {input.vcf} to avinput format \n"
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        /opt/annovar/convert2annovar.pl \
        -format vcf4 \
        -outfile {output.annovar} \
        {input.vcf}
        """

rule annovar :
    input:
        inav = "{outdir}/variant/{{sample}}_varscan.avinput".format(outdir=config["outdir"])
    output:
        all_vars ="{outdir}/variant/{{sample}}_varscan.avinput.variant_function".format(outdir=config["outdir"]),
        exonic_vars ="{outdir}/variant/{{sample}}_varscan.avinput.exonic_variant_function".format(outdir=config["outdir"])
    params:
        avdb = config["AVDB"],
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Annotating {input.inav} with Annovar \n"
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        /opt/annovar/annotate_variation.pl \
        --geneanno \
        --dbtype refGene \
        --buildver AV \
        {input.inav} \
        {params.avdb} \
        """
