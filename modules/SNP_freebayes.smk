__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


# SNP caller
#freebayes rule
rule freebayes:
    input:
        bam = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"])
        #on merged bams
        #bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    output:
        var = "{outdir}/variant/{{sample}}_freebayes.vcf".format(outdir=config["outdir"])
        #on merged bams
        #var = "{outdir}/variant/SNPs4all_freebayes.vcf".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        freebayes_bin = config["freebayes_bin"]
    message: "SNPs calling in {input.bam} using freebayes \n"
    shell:
        """
        zero=$(singularity exec {params.bind} {params.samtools_bin} samtools flagstat {input.bam}|head -n1|sed 's/\s.*//g')
        if [ "$zero" -lt "1" ]
        then
                echo "WARNING {output.var} EMPTY"
                touch {output.var}
                exit 0
        fi
        singularity exec {params.bind} {params.freebayes_bin} \
        freebayes -f {params.ref} \
        --min-mapping-quality 30 \
        --min-base-quality 20 \
        --min-alternate-fraction 0.15 \
        --min-coverage 8 \
        --use-duplicate-reads \
        {input.bam} > {output.var}
        exit 0
        """

