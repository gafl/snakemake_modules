# Repository for "reusable" & "interoperable" (almost) Snakemake modules

### All snakemake modules (rules) are in ./modules/
### 59 rules in 28 modules (smk)

## Template/example for:
### 1) Snakefile
    - Snakefile.smk  self-contained snakefile (all rules are included)
    - Snakefile_modules.smk uses external rules (include directive)

### 2) Configuration file in yaml format:, paths, singularity images paths, parameters,....
    - config.yaml

### 3) a sbatch file to run the pipeline: (to be edited)
    - run_snakemake_pipeline.slurm

### 4) Slurm directives file (time, cores, mem,...) in json format. Can be adjusted if needed
    - cluster.json

### 5) samples file in csv format
    Must contens at least 2 columns for SE reads and 3 for PE reads (tab separator )
    SampleName\tfq1\tfq2
    SampleName : your sample ID
    fq1: fastq file for a given sample
    fq2: read 2 for paired-end reads

    - samples.csv


### 6) R scripts call:
    - Example to run and pass arguments to a R script:         QTL_ezqtlseq.smk
    - Example to run and pass arguments to a Rmarkdown script: R_domarkdown.smk


### Documentation being written (still)
### Dev in progress

