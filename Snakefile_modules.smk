# Snakemake workflow example.
#
# Snakemake feature: fastq from csv file, config file in yaml, modules (rules), SLURM (json), report


import pandas as pd
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")


#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

cwd = os.getcwd() + "/"

# modules loading...
include : cwd + "smkmodules/modules/utils.smk"
include : cwd + "smkmodules/modules/PREPROC_fastp_pe.smk"
include : cwd + "smkmodules/modules/QC_fastqc_pe.smk"
include : cwd + "smkmodules/modules/QC_multiqc_fastqc.smk"
include : cwd + "smkmodules/modules/MAPPING_star2pass_pe.smk"
include : cwd + "smkmodules/modules/QC_multiqc_bam.smk"

rule final_outs:
    input:
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]),
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"]),
        expand("{outdir}/mapped/starpass2/{sample}/Aligned.sortedByCoord.out.bam", outdir=config["outdir"], sample=samples['SampleName']),
        "{outdir}/gene_count_dicoexpress.csv".format(outdir=config["outdir"])

